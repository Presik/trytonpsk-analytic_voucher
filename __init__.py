# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.

from trytond.pool import Pool
from . import voucher


def register():
    Pool.register(
        # voucher.VoucherLine,
        # voucher.Note,
        # voucher.NoteLine,
        # voucher.AnalyticAccountEntry,
        # voucher.AnalyticVoucherStart,
        module='analytic_voucher', type_='model')
    Pool.register(
        # voucher.AnalyticVoucher,
        module='analytic_voucher', type_='wizard')
    Pool.register(
        # voucher.AnalyticVoucherReport,
        module='analytic_voucher', type_='report')
