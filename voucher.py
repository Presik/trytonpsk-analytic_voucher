# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal

from trytond.pyson import Eval
from trytond.pool import PoolMeta, Pool
from trytond.modules.analytic_account import AnalyticMixin
from trytond.report import Report
from trytond.transaction import Transaction
from trytond.model import ModelView, fields
from trytond.wizard import Wizard, StateView, Button, StateReport
from trytond.exceptions import UserError
from trytond.i18n import gettext


STATES = {
    'required': False,
}

_ZERO = Decimal('0.0')


class VoucherLine(AnalyticMixin, metaclass=PoolMeta):
    __name__ = "account.voucher.line"

    @classmethod
    def __setup__(cls):
        super(VoucherLine, cls).__setup__()
        cls.analytic_accounts.states = STATES

    def get_analytic_entry(self, entry, line):
        analytic_entry = {}
        analytic_entry['debit'] = line.debit
        analytic_entry['credit'] = line.credit
        analytic_entry['account'] = entry.account.id
        analytic_entry['date'] = self.voucher.date
        return analytic_entry

    def _get_entry(self, line, analytic_lines):
        if self.analytic_accounts:
            to_create = []
            for entry in self.analytic_accounts:
                if not entry.account:
                    continue
                to_create.append(self.get_analytic_entry(entry, line))
            if to_create:
                line.write([line], {'analytic_lines': [('create', to_create)]})
        return line

    def get_move_line(self, move_id):
        move_line = super(VoucherLine, self).get_move_line(move_id)
        line = self._get_entry(move_line, [])
        return line


class Note(metaclass=PoolMeta):
    __name__ = "account.note"

    @classmethod
    def post(cls, records):
        for note in records:
            for line in note.lines:
                if line.analytic_account:
                    continue
                if line.account.code and line.account.code[0] in ('4', '5', '6', '7'):
                    raise UserError(gettext('Falta la cuenta analítica en una linea!'))
        super(Note, cls).post(records)


class NoteLine(AnalyticMixin, metaclass=PoolMeta):
    __name__ = "account.note.line"
    analytic_account = fields.Many2One('analytic_account.account',
        'Analytic Account', domain=[
            ('type', 'in', ['normal', 'distribution']),
            ('company', '=', Eval('context', {}).get('company', -1))
        ])

    @fields.depends('analytic_accounts', 'analytic_account')
    def on_change_analytic_account(self):
        if self.analytic_account:
            for ac in self.analytic_accounts:
                ac.account = self.analytic_account.id

    def get_analytic_entry(self, account, value):
        amount = value['debit'] or value['credit']
        lines = []
        for account, amount in account.distribute(amount):
            analytic_entry = {}
            analytic_entry['debit'] = amount if value['debit'] else Decimal(0)
            analytic_entry['credit'] = amount if value['credit'] else Decimal(0)
            analytic_entry['account'] = account
            analytic_entry['date'] = self.note.date
            lines.append(analytic_entry)
        return lines

    def _get_entry(self, value, analytic_lines):
        value['analytic_lines'] = []
        to_create = []
        if self.analytic_account:
            to_create.extend(self.get_analytic_entry(
                self.analytic_account, value)
            )
        elif self.analytic_accounts:
            for entry in self.analytic_accounts:
                if not entry.account:
                    continue
                to_create.extend(self.get_analytic_entry(entry.account, value))
        if to_create:
            value['analytic_lines'] = [('create', to_create)]
        return value

    def get_move_line(self):
        values = super(NoteLine, self).get_move_line()
        value = self._get_entry(values[0], [])
        return [value]


class AnalyticAccountEntry(metaclass=PoolMeta):
    __name__ = 'analytic.account.entry'

    @classmethod
    def _get_origin(cls):
        origins = super(AnalyticAccountEntry, cls)._get_origin()
        return origins + ['account.voucher.line', 'account.note.line']

    def on_change_with_required(self, name=None):
        result = super(AnalyticAccountEntry, self).on_change_with_required(name)
        if hasattr(self, 'origin') and self.origin \
                and self.origin.__name__ in ['account.note.line', 'account.voucher.line']:
            result = False
        return result


class AnalyticVoucherStart(ModelView):
    'Analytic Voucher Report Start'
    __name__ = 'analytic_report.voucher.start'
    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date("Start Date", required=True)
    end_date = fields.Date("End Date", required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()


class AnalyticVoucher(Wizard):
    'Analytic Voucheer Report'
    __name__ = 'analytic_report.voucher_wizard'
    start = StateView('analytic_report.voucher.start',
        'analytic_voucher.analytic_voucher_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('analytic_report.voucher_report')

    def do_print_(self, action):
        data = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
            }
        return action, data

    def transition_print_(self):
        return 'end'


class AnalyticVoucherReport(Report):
    __name__ = 'analytic_report.voucher_report'

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Company = pool.get('company.company')
        Lines = pool.get('account.voucher.line')

        lines = Lines.search([
            ('voucher.company', '=', data['company']),
            ('voucher.date', '>=', data['start_date']),
            ('voucher.date', '<=', data['end_date']),
            ('voucher.state', '=', 'posted'),
        ])

        voucher_lines_filtered = []

        for line in lines:
            name_analytic = ''
            _line = [line, ]
            if line.analytic_accounts:
                for a in line.analytic_accounts:
                    code_name = ''
                    if a.account:
                        code_name = a.account.name
                    name_analytic = name_analytic + code_name
            _line.append(name_analytic)
            voucher_lines_filtered.append(_line)

        report_context['records'] = voucher_lines_filtered
        report_context['company'] = Company(data['company'])
        report_context['start'] = data['start_date']
        report_context['end'] = data['end_date']

        return report_context
